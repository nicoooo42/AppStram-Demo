export const environment = {
    firebaseConfig: {
        apiKey: "AIzaSyBzxDPMshnxvecjOexfLIy4Y7w1AvFCHQA",
        authDomain: "authtest-24114.firebaseapp.com",
        databaseURL: "https://authtest-24114.firebaseio.com",
        projectId: "authtest-24114",
        storageBucket: "authtest-24114.appspot.com",
        messagingSenderId: "866728426126"
    },
    googleCloudVisionAPIKey: "googleCloudVisionAPIKey"
};
