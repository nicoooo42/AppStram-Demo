import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {UserService} from "../../providers/auth-service/user.service";
import {FirebaseUserModel} from "../../models/user.model";

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  user = new FirebaseUserModel();

  constructor(public navCtrl: NavController, public navParams: NavParams, public userService: UserService) {
      this.userService.getCurrentUser().then(res => {
          console.log(res);
          this.user.image = res.photoURL;
          this.user.name = res.displayName;
          this.user.provider = res.providerData[0].providerId;
          console.log(this.user);
      }, err => {
          console.log(err);
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

}
