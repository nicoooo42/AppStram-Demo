import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Camera, CameraOptions} from "@ionic-native/camera";


import { AlertController } from 'ionic-angular';
import { GoogleCloudVisionServiceProvider } from '../../providers/google-cloud-vision-service/google-cloud-vision-service';
import * as firebase from 'firebase';


@Component({
  selector: 'page-camera',
  templateUrl: 'camera.html',
})
export class CameraPage {

  constructor(private camera: Camera,
              private vision: GoogleCloudVisionServiceProvider,
              private alert: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CameraPage');
  }




    showAlert(message) {
        let alert = this.alert.create({
            title: 'Error',
            subTitle: message,
            buttons: ['OK']
        });
        alert.present();
    }


    takePhoto() {
        const options: CameraOptions = {
            quality: 100,
            targetHeight: 500,
            targetWidth: 500,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.PNG,
            mediaType: this.camera.MediaType.PICTURE
        }
        this.camera.getPicture(options).then((imageData) => {
            this.vision.getLabels(imageData).subscribe((result) => {
            }, err => {
                this.showAlert(err);
            });
        }, err => {
            this.showAlert(err);
        });
    }

}
