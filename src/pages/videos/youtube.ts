import { Component } from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {DomSanitizer} from "@angular/platform-browser";

@Component({
    selector: 'page-video',
    template: `
    <ion-header>
        <ion-navbar>
            <ion-title>Sci-fi</ion-title>
        </ion-navbar>
    </ion-header>
    
    <ion-content>
        <iframe width="100%" height="100%" [src]="iframe" frameborder="0" allowfullscreen></iframe>
    </ion-content>
    `
})
export class YoutubeVideo {

    iframe: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, private sanitizer: DomSanitizer) {
        this.iframe = sanitizer.bypassSecurityTrustResourceUrl(this.navParams.get('urlVideo'));
        console.log(this.iframe);
    }

}
