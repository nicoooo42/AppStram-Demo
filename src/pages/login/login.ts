import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {TabsPage} from "../tabs/tabs";
import {AuthService} from "../../providers/auth-service/auth.service";
import {YoutubeVideo} from "../videos/youtube";

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginPage {

    nextPage = TabsPage;

    constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthService) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad LoginPage');
    }

    tryFacebookLogin() {
        this.authService.doFacebookLogin()
            .then(res => {
                this.navCtrl.push(YoutubeVideo);
            })
    }

}
