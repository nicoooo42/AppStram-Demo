import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {YoutubeVideo} from "../videos/youtube";

@Component({
    selector: 'page-video',
    templateUrl: 'video.html'
})
export class VideoPage {

    constructor(public navCtrl: NavController) {

    }

    playYoutube(type) {

        let url: string;

        switch (type) {
            case 'lion': {
                url = 'https://www.youtube.com/embed/IGRLuiGTPLA'
                break;
            }
            case 'singe': {
                url = 'https://www.youtube.com/embed/63jqhx61cow'
                break;
            }
            default: {
                url = 'https://www.youtube.com/embed/A22oy8dFjqc'
                break;
            }
        }

        let urlVideo = {
            urlVideo: url
        }

        this.navCtrl.push(YoutubeVideo, urlVideo);
    }

}
