import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { MusicPage } from '../pages/music/music';
import { VideoPage } from '../pages/video/video';
import { NewsPage } from '../pages/news/news';
import { TabsPage } from '../pages/tabs/tabs';
import { IntroPage} from "../pages/intro/intro";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {LoginPage} from "../pages/login/login";
import {YoutubeVideo} from "../pages/videos/youtube";
import {CameraPage} from "../pages/camera/camera";
import {Camera} from "@ionic-native/camera";
import {GalleryPage} from "../pages/gallery/gallery";
import {DescriptionPage} from "../pages/description/description";

import { IonicImageViewerModule } from 'ionic-img-viewer';

import { HttpModule } from '@angular/http';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';



import { environment } from '../environement';
import { GoogleCloudVisionServiceProvider } from '../providers/google-cloud-vision-service/google-cloud-vision-service';
import { AuthService } from '../providers/auth-service/auth.service';
import {ProfilePage} from "../pages/profile/profile";
import {UserService} from "../providers/auth-service/user.service";

@NgModule({
  declarations: [
    MyApp,
    MusicPage,
    VideoPage,
    NewsPage,
    TabsPage,
    IntroPage,
    LoginPage,
    YoutubeVideo,
    CameraPage,
    DescriptionPage,
      GalleryPage,
      ProfilePage
  ],
  imports: [
    BrowserModule,
      IonicImageViewerModule,
      HttpModule,
    IonicModule.forRoot(MyApp),
      AngularFireModule.initializeApp(environment.firebaseConfig),
      AngularFireDatabaseModule,
      AngularFireAuthModule,
      AngularFirestoreModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MusicPage,
    VideoPage,
    NewsPage,
    TabsPage,
    IntroPage,
    LoginPage,
    YoutubeVideo,
    CameraPage,
    DescriptionPage,
      GalleryPage,
      ProfilePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
      Camera,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    GoogleCloudVisionServiceProvider,
      AuthService,
      UserService
  ]
})
export class AppModule {}
